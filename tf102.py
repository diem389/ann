import os
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf

pop_size = 100
features = 50
selection = 0.2
mutation = 1./features
generations = 200
num_parents = int(pop_size*selection)
num_children = pop_size - num_parents

sess = tf.Session()
ftype = np.float32

# Ground truth
truth = np.sin(2*np.pi*(np.arange(features, dtype=ftype)))

population = tf.Variable(np.random.randn(pop_size, features), dtype=ftype)
truth_ph = tf.placeholder(tf.float32, [1, features])
crossover_mat_ph = tf.placeholder(ftype, [num_children, features])
mutation_val_ph = tf.placeholder(ftype, [num_children, features])

fitness = -tf.reduce_mean(tf.sqaure(tf.sub(population, truth_ph)), 1)
top_vals, top_int = tf.nn.top_k(fitness, k=pop_size)





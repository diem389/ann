from tensorflow.python.keras.datasets import imdb
from keras import models
from keras import layers
import numpy as np
import matplotlib.pyplot as plt

(train_data, train_labels), (test_data, test_labels) =imdb.load_data(num_words=10000)

# dic word --> index
word2index = imdb.get_word_index()
index2word = dict([(value, key) for (key, value) in word2index.items()])


def decode_review(review):
    return " ".join([index2word.get(i-3, '?') for i in review])

print(decode_review(train_data[3]))
print(decode_review(train_data[5]))


def vectorize_sequence(sequences, dimension=10000):
    results = np.zeros((len(sequences), dimension), dtype='float32')
    for i, sequence in enumerate(sequences):
        results[i, sequence] = 1.0
    return results


x_train = vectorize_sequence(train_data)
x_test = vectorize_sequence(test_data)
y_train = np.asarray(train_labels).astype('float32')
y_test = np.asarray(test_labels).astype('float32')


input_tensor = layers.Input(shape=(10000, ))

x = layers.Dense(16, activation='relu')(input_tensor)
x = layers.Dropout(0.5)(x)
x = layers.Dense(16, activation='relu')(x)
x = layers.Dropout(0.5)(x)
output = layers.Dense(1, activation='sigmoid')(x)

model = models.Model(inputs=input_tensor, outputs=output)
model.summary()

model.compile(optimizer='rmsprop', loss='binary_crossentropy',
              metrics=['accuracy'])

history = model.fit(x_train, y_train, epochs=10, batch_size=512,
                    validation_split=0.2)

# Result check
def draw_result(history_dict):
    plt.clf()

    loss_values = history_dict['loss']
    val_loss_values = history_dict['val_loss']
    acc = history_dict['acc']
    val_acc = history_dict['val_acc']

    epochs = range(1, len(loss_values) + 1)

    plt.subplot(2, 1, 1)
    plt.plot(epochs, loss_values, 'bo', label='Training loss')
    plt.plot(epochs, val_loss_values, 'b', label='Validation loss')

    plt.title('Training Loss & Acc.')
    plt.ylabel('Loss')
    plt.legend()

    plt.subplot(2, 1, 2)

    plt.plot(epochs, acc, 'ro', label='Training accuracy')
    plt.plot(epochs, val_acc, 'r', label='Validation accuracy')

    # plt.title('Training validation accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.legend()

    plt.show()


draw_result(history.history)
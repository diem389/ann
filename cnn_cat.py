from keras.layers import Input, Conv2D, \
    MaxPooling2D, Dense, Flatten, Dropout
from keras.models import Model
from keras import optimizers

import catdog_dt as pre


def cnn_model(img_row=150, img_col=150, img_channel=3):
    input = Input((img_row, img_col, img_channel))

    conv1 = Conv2D(32, (3, 3), activation='relu')(input)
    conv1 = MaxPooling2D((2, 2))(conv1)

    conv2 = Conv2D(64, (3, 3), activation='relu')(conv1)
    conv2 = MaxPooling2D((2, 2))(conv2)

    conv3 = Conv2D(128, (3, 3), activation='relu')(conv2)
    conv3 = MaxPooling2D((2, 2))(conv3)

    conv4 = Conv2D(256, (3, 3), activation='relu')(conv3)
    conv4 = MaxPooling2D((2, 2))(conv4)

    output = Flatten()(conv4)
    output = Dropout(0.5)(output)
    output = Dense(512, activation='relu')(output)
    output = Dense(1, activation='sigmoid')(output)
    model = Model(inputs=input, outputs=output)

    model.summary()
    model.compile(optimizer=optimizers.RMSprop(lr=1e-4),
                  loss='binary_crossentropy',
                  metrics=['accuracy'])
    return model


model = cnn_model()

from keras.preprocessing.image import ImageDataGenerator

train_data_gen = ImageDataGenerator(rescale=1./255, rotation_range=40, width_shift_range=0.2,
                                   height_shift_range=0.2, shear_range=0.2, zoom_range=0.2,
                                   horizontal_flip=True, fill_mode='nearest')

test_data_gen = ImageDataGenerator(rescale=1./255)

train_generator = train_data_gen.flow_from_directory(
    pre.train_dir, target_size=(150, 150), batch_size=64,
    class_mode='binary')

validation_generator = test_data_gen.flow_from_directory(
    pre.valid_dir, target_size=(150, 150), batch_size=20,
    class_mode='binary')

history = model.fit_generator(train_generator,
                              steps_per_epoch=100, epochs=30,
                              validation_data=validation_generator,
                              validation_steps=50)

model.save('cats_dogs_small_2.h5')

# Result check
def draw_result(history_dict):
    import matplotlib.pyplot as plt
    plt.clf()

    loss_values = history_dict['loss']
    val_loss_values = history_dict['val_loss']
    acc = history_dict['acc']
    val_acc = history_dict['val_acc']

    epochs = range(1, len(loss_values) + 1)

    plt.subplot(2, 1, 1)
    plt.plot(epochs, loss_values, 'bo', label='Training loss')
    plt.plot(epochs, val_loss_values, 'b', label='Validation loss')

    plt.title('Training Loss & Acc.')
    plt.ylabel('Loss')
    plt.legend()

    plt.subplot(2, 1, 2)

    plt.plot(epochs, acc, 'ro', label='Training accuracy')
    plt.plot(epochs, val_acc, 'r', label='Validation accuracy')

    # plt.title('Training validation accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.legend()

    plt.show()

draw_result(history.history)
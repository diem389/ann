import os
import numpy 
import SimpleITK
import matplotlib.pyplot as plt


# Global Variables
# - options
pathDicom = "E:/OneDrive/MedicalData/MyHead/"
idxSlice = 50

labelWhiteMatter = 1
labelGrayMatter = 2


def sitk_show(img, title=None, margin=0.05, dpi=40):
    nda = SimpleITK.GetArrayFromImage(img)
    spacing = img.GetSpacing()
    figsize = (1 + margin) * nda.shape[0] / dpi, (1 + margin) * nda.shape[1] / dpi
    extent = (0, nda.shape[1] * spacing[1], nda.shape[0] * spacing[0], 0)
    fig = plt.figure(figsize=figsize, dpi=dpi)
    ax = fig.add_axes([margin, margin, 1 - 2 * margin, 1 - 2 * margin])

    plt.set_cmap("gray")
    ax.imshow(nda, extent=extent, interpolation=None)

    if title:
        plt.title(title)

    plt.show()



if __name__ == "__main__":

    # DICOM Input
    reader = SimpleITK.ImageSeriesReader()
    filenamesDicom = reader.GetGDCMSeriesFileNames(pathDicom)
    reader.SetFileNames(filenamesDicom)
    imgOriginal = reader.Execute()
    # Limit image to a slice 2D

    aSlice = imgOriginal[:, :, idxSlice]
    sitk_show(aSlice)

    # Smoothing
    imgSmooth = SimpleITK.CurvatureFlow(image1=aSlice,
                                        timeStep=0.124, numberOfIterations=5)
    sitk_show(imgSmooth)

    #Initial segmentation of white matter
    seeds = [(150, 75)]
    imgWithMatter = SimpleITK.ConnectedThreshold(image1=imgSmooth, seedList=seeds,
                                                 lower=130, upper=190,
                                                 replaceValue=labelWhiteMatter)

    # Visiualization with label overlay
    imgSmoothInt = SimpleITK.Cast(SimpleITK.RescaleIntensity(imgSmooth),
                                  imgWithMatter.GetPixelID())
    sitk_show(SimpleITK.LabelOverlay(imgSmoothInt, imgWithMatter))
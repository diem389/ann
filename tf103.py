import tensorflow as tf
from tensorflow import keras

import numpy as np
import matplotlib.pyplot as plt


# Casting
x = tf.constant([1, 2, 3], name='x', dtype=tf.float32)
print(x.dtype)

x = tf.cast(x, tf.int64)
print(x.dtype)

# Interactive
sess = tf.InteractiveSession()
c = tf.linspace(0.0, 4.0, 5)
print(f"The constant of c: {c.eval()}")
sess.close()


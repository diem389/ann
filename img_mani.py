import cv2

FILE_PATH = "e:/OneDrive/data_ai/test_24_32.bmp"
img = cv2.imread(FILE_PATH)
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
img = img.reshape(img.shape + (1,))

print("img shape: ", img.shape)

# import matplotlib.pyplot as plt
# plt.imshow(img, cmap='gray')
# plt.show()

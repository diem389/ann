from __future__ import division, print_function

import collections
import os
import random

import numpy as np
import pygame
from keras.layers import Conv2D, Dense, Flatten, Input
from keras.models import Model
from keras.optimizers import Adam
from scipy.misc import imresize


class WrappedGame1:
    # Headless mode
    def __init__(self, no_screen=False):

        if no_screen:
            os.environ['SDL_VIDEODRIVER'] = 'dummy'

        pygame.init()

        # set constants
        self.COLOR_WHITE = (255, 255, 255)
        self.COLOR_BLACK = (0, 0, 0)
        self.GAME_WIDTH = 400
        self.COLOR_BLACK = (0, 0, 0)
        self.GAME_HEIGHT = 400
        self.BALL_WIDTH = 20
        self.BALL_HEIGHT = 20
        self.PADDLE_WIDTH = 50
        self.PADDLE_HEIGHT = 10
        self.GAME_FLOOR = 350
        self.GAME_CEILING = 10

        self.BALL_VELOCITY = 10
        self.PADDLE_VELOCITY = 20
        self.FONT_SIZE = 30
        self.MAX_TRIES_PER_GAME = 1
        self.CUSTOM_EVENT = pygame.USEREVENT + 1
        self.font = pygame.font.SysFont("Comic Sans MS", self.FONT_SIZE)

    def reset(self):
        self.frames = collections.deque(maxlen=4)
        self.game_over = False
        self.paddle_x = self.GAME_WIDTH // 2
        self.game_score = 0
        self.reward = 0
        self.ball_x = random.randint(0, self.GAME_WIDTH - self.BALL_WIDTH)
        self.ball_y = self.GAME_CEILING
        self.num_tries = 0

        # display setup
        self.screen = pygame.display.set_mode(
            (self.GAME_WIDTH, self.GAME_HEIGHT))
        self.clock = pygame.time.Clock()

    def get_frames(self):
        return np.array(list(self.frames))

    def step(self, action):
        pygame.event.pump()

        if action == 0:
            self.paddle_x -= self.PADDLE_VELOCITY
            if self.paddle_x < 0:
                self.paddle_x = self.PADDLE_VELOCITY

        elif action == 2:
            self.paddle_x += self.PADDLE_VELOCITY
            if self.paddle_x > self.GAME_WIDTH - self.PADDLE_WIDTH:
                self.paddle_x = self.GAME_WIDTH - self.PADDLE_WIDTH \
                                - self.PADDLE_VELOCITY
        else:
            pass

        self.screen.fill(self.COLOR_BLACK)
        score_text = self.font.render(
            f"Score: {self.game_score:d}/{self.MAX_TRIES_PER_GAME:d}," +
            f" Ball: {self.num_tries:d}", True, self.COLOR_WHITE)

        self.screen.blit(score_text,
                         ((self.GAME_WIDTH - score_text.get_width()) // 2,
                          (self.GAME_FLOOR + self.FONT_SIZE // 2)))

        # update ball position
        self.ball_y += self.BALL_VELOCITY  # Just falling down
        ball = pygame.draw.rect(self.screen, self.COLOR_WHITE,
                                pygame.Rect(self.ball_x, self.ball_y,
                                            self.BALL_WIDTH, self.BALL_HEIGHT))
        # update paddle pos.
        paddle = pygame.draw.rect(self.screen, self.COLOR_WHITE,
                                  pygame.Rect(self.paddle_x, self.GAME_FLOOR,
                                              self.PADDLE_WIDTH,
                                              self.PADDLE_HEIGHT))
        # Check for collision & update reward
        self.reward = 0
        if self.ball_y >= self.GAME_FLOOR - self.BALL_WIDTH // 2:
            if ball.colliderect(paddle):
                self.reward = 1
            else:
                self.reward = -1

            self.game_score += self.reward
            self.ball_x = random.randint(0, self.GAME_WIDTH)
            self.ball_y = self.GAME_CEILING
            self.num_tries += 1

        pygame.display.flip()

        # save last 4 games
        self.frames.append(pygame.surfarray.array2d(self.screen))
        if self.num_tries >= self.MAX_TRIES_PER_GAME:
            self.game_over = True

        self.clock.tick(30)
        return np.array(list(self.frames)), self.reward, self.game_over


def preprocess_images(images):
    if images.shape[0] < 4:
        x_t = images[0]
        x_t = imresize(x_t, (80, 80))
        x_t = x_t.astype("float32")
        x_t /= 255.0
        s_t = np.stack((x_t, x_t, x_t, x_t), axis=2)
    else:
        xt_list = []
        for i in range(images.shape[0]):
            x_t = imresize(images[i], (80, 80))
            x_t = x_t.astype('float32')
            x_t /= 255.0
            xt_list.append(x_t)

        s_t = np.stack((xt_list[k] for k in range(4)), axis=2)

    s_t = np.expand_dims(s_t, axis=0)
    return s_t


def get_next_batch(experience, model, num_actions, gamma, batch_size):
    batch_indices = np.random.randint(low=0, high=len(experience),
                                      size=batch_size)
    batch = [experience[i] for i in batch_indices]
    X = np.zeros((batch_size, 80, 80, 4))
    Y = np.zeros((batch_size, num_actions))
    for i in range(len(batch)):
        s_t, a_t, r_t, s_tp1, game_over = batch[i]
        X[i] = s_t
        Y[i] = model.predict(s_t)[0]
        Q_sa = np.max(model.predict(s_tp1)[0])
        if game_over:
            Y[i, a_t] = r_t
        else:
            Y[i, a_t] = r_t + gamma * Q_sa

    return X, Y


def cnn_model(img_row=80, img_col=80, img_channel=4):
    input = Input((img_row, img_col, img_channel))

    conv1 = Conv2D(32, 8, strides=4, padding='same', activation='relu')(input)
    conv2 = Conv2D(64, 4, strides=2, padding='same', activation='relu')(conv1)
    conv3 = Conv2D(64, 3, strides=1, padding='same', activation='relu')(conv2)
    output = Flatten()(conv3)
    output = Dense(512, activation='relu')(output)
    output = Dense(3)(output)

    model = Model(inputs=input, outputs=output)
    model.summary()

    model.compile(optimizer=Adam(lr=1e-6), loss='mse')
    return model


if __name__ == "__main__":
    game = WrappedGame1()

    NUM_EPOCHS = 10
    for e in range(NUM_EPOCHS):
        print("Epoch: {:d}".format(e))
        game.reset()
        input_t = game.get_frames()
        game_over = False
        while not game_over:
            action = np.random.randint(0, 3, size=1)[0]
            input_tp1, reward, game_over = game.step(action)
            print(action, reward, game_over)

# https://github.com/lazyprogrammer/machine_learning_examples/blob/master/cnn_class2/use_pretrained_weights_vgg.py

from keras.layers import Input, Lambda, Dense, Flatten
from keras.models import Model
from keras.applications.vgg16 import VGG16
from keras.applications.vgg16 import preprocess_input
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator

from sklearn.metrics import confusion_matrix
import numpy as np
import matplotlib.pyplot as plt

from glob import glob
from util import plot_confusion_matrix


# Sub Modules
def get_confusion_matrix(data_path, N):
    print("Generating confusion matrix", N)
    predictions = []
    targets = []

    i = 0
    for x, y in gen.flow_from_directory(data_path, target_size=IMAGE_SIZE, shuffle=False,
                                        batch_size=batch_size * 2):
        i += 1
        if i % 50 == 0:
            print(i)

        p = model.predict(x)
        p = np.argmax(p, axis=1)
        y = np.argmax(y, axis=1)
        predictions = np.concatenate((predictions, p))
        targets = np.concatenate((targets, y))
        if len(targets) >= N:
            break

    cm = confusion_matrix(targets, prediction)
    return cm


# resize all the images
IMAGE_SIZE = [100, 100]

epochs = 2
batch_size = 64

#root_path = "E:/data/"
root_path = "/mnt/sda1/data/"
train_path = root_path + "fruits-360/Training"
valid_path = root_path + "fruits-360/Test"

image_files = glob(train_path + '/*/*.jp*g')
valid_image_files = glob(valid_path + '/*/*.jp*g')

folders = glob(train_path + '/*')

plt.imshow(image.load_img(np.random.choice(image_files)))
plt.show()


vgg = VGG16(input_shape=IMAGE_SIZE + [3], weights='imagenet',
            include_top=False)

for layer in vgg.layers:
    layer.trainable = False

# Our layer
x = Flatten()(vgg.output)
prediction = Dense(len(folders), activation='softmax')(x)

# create a model object
model = Model(inputs=vgg.input, outputs=prediction)
model.summary()

model.compile(loss='categorical_crossentropy',
              optimizer='rmsprop', metrics=['accuracy'])

gen = ImageDataGenerator(
    rotation_range=20,
    width_shift_range=0.1,
    height_shift_range=0.1,
    shear_range=0.1,
    zoom_range=0.2,
    horizontal_flip=True,
    vertical_flip=True,
    preprocessing_function=preprocess_input
)

test_gen = gen.flow_from_directory(valid_path, target_size=IMAGE_SIZE)
print(test_gen.class_indices)
labels = [None] * len(test_gen.class_indices)
for k, v in test_gen.class_indices.items():
    labels[v] = k

for x, y in test_gen:
    print("min: ", x[0].min(), "max: ", x[0].max())
    plt.title(labels[np.argmax(y[0])])
    plt.imshow(x[0])
    plt.show()
    break

# Create Generators
train_generator = gen.flow_from_directory(train_path,
                                          target_size=IMAGE_SIZE,
                                          shuffle=True,
                                          batch_size=batch_size)

# fit the model
r = model.fit_generator(train_generator, validation_data=test_gen,
                        epochs=epochs, steps_per_epoch=len(image_files) // batch_size,
                        validation_steps=len(valid_image_files) // batch_size)

# cm = get_confusion_matrix(train_path, len(image_files))
# print(cm)
# valid_cm = get_confusion_matrix(valid_path, len(valid_image_files))
# print(valid_cm)

# Plot some data
# - loss
plt.plot(r.history['loss'], label='train loss')
plt.plot(r.history['val_loss'], label='val loss')
plt.legend()
plt.show()

# - accuracies
plt.plot(r.history['acc'], label='train acc')
plt.plot(r.history['val_acc'], label='val acc')
plt.legend()
plt.show()
#
# plot_confusion_matrix(cm, labels, title='Train Confusion Matrix')
# plot_confusion_matrix(valid_cm, labels, title='Validation confusion matrix')

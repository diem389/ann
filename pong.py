import numpy as np
import gym
import tensorflow as tf
import matplotlib.pyplot as plt

# Env
env = gym.make("Pong-v0")
observation = env.reset()

for i in range(22):
    if i > 20:
        plt.imshow(observation)
        plt.show()
    observation, _, _, _ = env.step(1)


def preprocess_frame(frame):
    frame = frame[35:195, 10:150]
    # gray scale & downside by half
    frame = frame[::2, ::2, 0]
    frame[frame == 144] = 0
    frame[frame == 109] = 0
    # set ball and paddles to 1
    frame[frame != 0] = 1
    return frame.astype(np.float).ravel()


# chekc preprocess
obs_preprocessed = preprocess_frame(observation).reshape(80, 70)
plt.imshow(obs_preprocessed, cmap='gray')
plt.show()

# Observation next
observation_next, _, _, _ = env.step(1)
diff = preprocess_frame(observation_next) - preprocess_frame(observation)
plt.imshow(diff.reshape(80, 70), cmap='gray')
plt.show()

# Training
input_dim = 80*70
hidden_L1 = 400
hidden_L2 = 200
actions = [1, 2, 3]
n_actions = len(actions)

model = {}
with tf.variable_scope('L1', reuse=False):
    init_W1 = tf.truncated_normal_initializer(mean=0, stddev=1./np.sqrt)
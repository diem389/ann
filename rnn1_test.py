from keras.models import Sequential
from keras.layers import Dense, LSTM, Dropout

from rnn1 import create_regressor, generate_xy
import pandas as pd
from sklearn.preprocessing import MinMaxScaler

if __name__=="__main__":

    dataset_train = pd.read_csv('data/Google_Stock_Price_Train.csv')
    training_set = dataset_train.iloc[:, 1:2].values
    X_trains, y_trains, sc = generate_xy(training_set)

    dataset_test = pd.read_csv('data/Google_Stock_Price_Test.csv')
    read_stock_price = dataset_test.iloc[:, 1:2].values
    dataset_train = pd.read_csv('data/Google_Stock_Price_Train.csv')

    # Concatenating training & test
    dataset_total = pd.concat((dataset_train['Open'], dataset_test['Open']), axis=0)
    inputs = dataset_total[len(dataset_total) - len(dataset_test) - 60:].values
    inputs = inputs.reshape(-1, 1)
    inputs = sc.transform(inputs)

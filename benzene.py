from keras.layers import Input
from keras.layers.core import Dense

from sklearn.preprocessing import StandardScaler
from keras.models import Model, load_model

import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd

DATA_DIR = "e:/OneDrive/data_ai"
AIR_QUALITY_FILE = os.path.join(DATA_DIR, "AirQualityUCI.csv")

aqdf = pd.read_csv(AIR_QUALITY_FILE, sep=";", decimal=",", header=0)

# Remove first and last 2 columns
del aqdf["Date"]
del aqdf["Time"]
del aqdf["Unnamed: 15"]
del aqdf["Unnamed: 16"]

# Fill Nans in each column with the mean value
aqdf = aqdf.fillna(aqdf.mean())

Xorig = aqdf.as_matrix()
scaler = StandardScaler()
Xscaled = scaler.fit_transform(Xorig)

Xmeans = scaler.mean_
Xstds = scaler.scale_

y = Xscaled[:, 3]
X = np.delete(Xscaled, 3, axis=1)

# Splitting
train_size = int(0.7 * X.shape[0])
Xtrain, Xtest, ytrain, ytest = X[0:train_size], X[train_size:], \
                               y[0: train_size], y[train_size:]

# Network: 2 Layer, 12 features input, a scaled prediction
# loss function: MSE
# optimization: Adam

KERNEL_INIT = "glorot_uniform"
readings = Input(shape=(12,))
x = Dense(8, activation="relu",
          kernel_initializer=KERNEL_INIT)(readings)
benzene = Dense(1, kernel_initializer=KERNEL_INIT)(x)
model = Model(inputs=[readings], outputs=[benzene])
model.compile(loss="mse", optimizer="adam")

# Train

NUM_EPOCHS = 20
BATCH_SIZE = 10

history = model.fit(Xtrain, ytrain,
                    batch_size=BATCH_SIZE,
                    epochs=NUM_EPOCHS, validation_split=0.2)

model.save(os.path.join(DATA_DIR, "benzene.h5"))
del model

def predict_benzene(path=None):
    if path is None:
        path = os.path.join(DATA_DIR, "benzene.h5")

    m = load_model(path)

    ytest_ = m.predict(Xtest).flatten()
    for i in range(10):
        label = (ytest[i]* Xstds[3]) + Xmeans[3]
        prediction = (ytest_[i]*Xstds[3]) + Xmeans[3]
        print(f"Benzene Conc. expected: {label:.3f}," +
            f"predicted {prediction:.3f}")


predict_benzene()
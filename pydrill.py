

class Contact:
    def __init__(self):
        print("Contact created")

    def __call__(self, *args, **kwargs):
        print("Called:", args, kwargs)

c = Contact()
c("hi")

input("Push any button to start processing.... ")

from tensorflow.contrib.rnn import OutputProjectionWrapper
from tensorflow.nn.rnn_cell import BasicRNNCell
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

t_min, t_max = 0, 30
resolution = 0.1

# Util Modules
def reset_graph(seed=11):
    tf.reset_default_graph()
    tf.set_random_seed(seed)
    np.random.seed(seed)

def time_series(t):
    return t*np.sin(t)/3 + 2*np.sin(t*5)


def next_batch(batch_size, n_steps):
    t0 = np.random.rand(batch_size, 1)*(t_max-t_min-n_steps*resolution)
    Ts = t0 + np.arange(0., n_steps + 1)*resolution
    ys = time_series(Ts)
    return ys[:, :-1].reshape(-1, n_steps, 1), \
        ys[:, 1:].reshape(-1, n_steps, 1)


#
n_steps = 20
n_neurons = 100
n_inputs = 1
n_outputs = 1

t = np.linspace(t_min, t_max, int((t_max-t_min)/resolution))
t_instance = np.linspace(12.2, 12.2 + resolution * (n_steps + 1), n_steps + 1)

print("t = ")
print(t)

print("t_instance = ")
print(t_instance)

# plot


def plot_preview(t, t_instance, image_file="preview.png"):
    plt.figure(figsize=(11, 4))
    plt.subplot(121)
    plt.title('A time series (generated)', fontsize=14)
    plt.plot(t, time_series(t), label="t*sin(t)/3 + 2*sin(5t)")
    plt.plot(t_instance[:-1], time_series(t_instance[:-1]), "b-",
             linewidth=3, label="A training instance")
    plt.legend(loc='lower left', fontsize=14)
    plt.axis([0, 30, -17, 13])
    plt.xlabel("Time")
    plt.ylabel("Value")

    plt.subplot(122)
    plt.title('A training instance', fontsize=14)
    plt.plot(t_instance[:-1], time_series(t_instance[:-1]), "bo",
             markersize=10, label="instance")
    plt.plot(t_instance[1:], time_series(t_instance[1:]), "ro",
             markersize=10, label="target")
    plt.legend(loc='upper left', fontsize=14)
    plt.xlabel("Time")
    plt.show()
    plt.savefig(image_file, format='png', dpi=300)


#
X_batch, y_batch = next_batch(1, n_steps)

X = tf.placeholder(tf.float32, [None, n_steps, n_inputs])
y = tf.placeholder(tf.float32, [None, n_steps, n_outputs])


cell = OutputProjectionWrapper(BasicRNNCell(
    num_units=n_neurons, activation=tf.nn.relu), output_size=n_outputs)
outputs, states = tf.nn.dynamic_rnn(cell, X, dtype=tf.float32)

learning_rate = 0.001
loss = tf.reduce_mean(tf.square(outputs-y))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
training_op = optimizer.minimize(loss)

init = tf.global_variables_initializer()
saver = tf.train.Saver()
n_iterations = 1500
batch_size = 50

with tf.Session() as sess:
    init.run()
    for iteration in range(n_iterations):
        X_batch, y_batch = next_batch(batch_size, n_steps)
        sess.run(training_op, feed_dict={X:X_batch, y:y_batch})
        if iteration % 100 == 0:
            mse = loss.eval(feed_dict={X:X_batch, y: y_batch})
            print(iteration, "\tMSE: ", mse)

    saver.save(sess, "./time_series_model")


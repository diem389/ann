import k3d
plot = k3d.plot()

vertices = [[0, 0, 0], [0, 0, 1], [1, 0, 0]]
indices = [[0, 1, 2]]

mesh = k3d.mesh(vertices, indices)
plot += mesh

plot.display()
from keras.models import Model
from keras import optimizers
from keras.applications import VGG16
import graph_util as gu
from keras.layers import Dense, Flatten, Dropout

import catdog_dt as dt
from keras.preprocessing.image import ImageDataGenerator


def cnn_model_for_vgg2(input_dim):
    conv_base = VGG16(weights='imagenet', include_top=False,
                      input_shape=input_dim)
    conv_base.trainable = False

    x = Flatten()(conv_base.output)
    x = Dropout(0.5)(x)
    x = Dense(256, activation='relu')(x)
    x = Dense(1, activation='sigmoid')(x)

    m = Model(input=conv_base.input, output=x)
    m.summary()

    # make some constraints
    m.compile(optimizer=optimizers.RMSprop(lr=1.0e-5),
              loss='binary_crossentropy', metrics=['acc'])
    return m


model = cnn_model_for_vgg2((150, 150, 3))

train_data_gen = ImageDataGenerator(rescale=1. / 255, rotation_range=40, width_shift_range=0.2,
                                    height_shift_range=0.2, shear_range=0.2, zoom_range=0.2,
                                    horizontal_flip=True, fill_mode='nearest')

test_data_gen = ImageDataGenerator(rescale=1. / 255)

train_generator = train_data_gen.flow_from_directory(
    dt.train_dir, target_size=(150, 150), batch_size=20,
    class_mode='binary')

validation_generator = test_data_gen.flow_from_directory(
    dt.valid_dir, target_size=(150, 150), batch_size=20,
    class_mode='binary')

history = model.fit_generator(train_generator,
                              steps_per_epoch=100, epochs=30,
                              validation_data=validation_generator,
                              validation_steps=50)

model.save('cats_dogs_small_3.h5')

gu.draw_result(history.history)

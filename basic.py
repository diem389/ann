# mnist test

import os
import keras
from keras.datasets import mnist

from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.optimizers import Adam

from keras.utils import np_utils
from keras.callbacks import ModelCheckpoint


class LossHistory(keras.callbacks.Callback):
    def on_train_begin(self, logs=None):
        self.losses = []

    def on_batch_end(self, batch, logs=None):
        self.losses.append(logs.get('loss'))


if __name__ =='__main__':

    # network and training
    NB_EPOCH = 20
    BATCH_SIZE = 128
    VERBOSE = 1
    NB_CLASSES = 10
    OPTIMIZER = Adam()
    N_HIDDEN = 128
    VALIDATION_SPLIT = 0.2
    DROP_OUT = 0.3
    MODEL_DIR = "/tmp"

    # Data
    (X_train, y_train), (X_test, y_test) = mnist.load_data()
    # X_train is 60000 rows of 28x28 values
    RESHAPED = 784
    TRAIN_NUM = X_train.shape[0]
    TEST_NUM = X_test.shape[0]

    X_train = X_train.reshape(TRAIN_NUM, RESHAPED)
    X_test = X_test.reshape(TEST_NUM, RESHAPED)
    X_train = X_train.astype('float32')
    X_test = X_test.astype('float32')

    # normalize images
    X_train /= 255.0
    X_test /= 255.0

    # Convert class vectors to binary class matrices
    Y_train = np_utils.to_categorical(y_train, NB_CLASSES)
    Y_test = np_utils.to_categorical(y_test, NB_CLASSES)

    #
    model = Sequential()
    model.add(Dense(N_HIDDEN, input_shape=(RESHAPED,)))
    model.add(Activation('relu'))
    model.add(Dropout(DROP_OUT))

    model.add(Dense(N_HIDDEN))
    model.add(Activation('relu'))
    model.add(Dropout(DROP_OUT))

    model.add(Dense(NB_CLASSES))
    model.add(Activation('softmax'))

    model.summary()

    #
    model.compile(loss='categorical_crossentropy', optimizer=OPTIMIZER, metrics=['accuracy'])

    checkpoint = ModelCheckpoint(filepath=os.path.join(MODEL_DIR, "model-{epoch:02d}.h5"),
                                 save_best_only=True)

    history = model.fit(X_train, Y_train, batch_size=BATCH_SIZE, epochs=NB_EPOCH,
                        verbose=VERBOSE, validation_split=VALIDATION_SPLIT, callbacks=[checkpoint])

    score = model.evaluate(X_test, Y_test, verbose=VERBOSE)

    print("One Hidden Layer Net Result...")
    print("Test score:", score[0])
    print(f"Test accuracy: {score[1]*100} %")






import gym
import random
import numpy as np 
import matplotlib.pyplot as plt
from collections import deque

from keras.models import Sequential
from keras.layers import Dense, Flatten, Conv2D
from keras.optimizers import Adam


# 
env = gym.make('BreakoutDeterministic-v4')
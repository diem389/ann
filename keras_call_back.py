from keras import Sequential
from keras.layers import Dense, Activation
import keras

class LossHistory(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.losses = []

    def on_batch_end(self, batch, logs={}):
        self.losses.append(logs.get('loss'))


model = Sequential()
model.add(Dense(10, input_dim=784, init='uniform'))
model.add(Activation('softmax'))
model.compile(loss='categorical_crossentropy', optimizer='rmsprop')

# dummy
X_train = []
Y_train = []

history = LossHistory()
model.fit(X_train, Y_train, batch_size=128, nb_epoch=20,
          verbose=0, callbacks=[history])
print(history.losses)

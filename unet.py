import glob
import os
import time
import pickle

import cv2
import matplotlib.image as mi
import matplotlib.pyplot as plt
import numpy as np
from keras import backend as K

from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from keras.layers import Input, concatenate, Conv2D
from keras.layers import MaxPooling2D, UpSampling2D, Dropout
from keras.models import Model
from keras.optimizers import Adam

data_root_folder = "e:\\OneDrive\\data_ai\\carvana_unet\\"
train_data_folder = "train"
train_mask_folder = "train_masks"
test_data_folder = "test"

count = 0

train_image_files = glob.glob(data_root_folder + train_data_folder + "/*.jp*g")
train_mask_files = glob.glob(data_root_folder + train_mask_folder + "/*.gif")

n_examples = 5
for i in range(n_examples):
    idx = np.random.randint(0, len(train_mask_files) - 1)

    plt.subplot(n_examples, 2, i * 2 + 1)
    image = mi.imread(train_image_files[idx])
    plt.imshow(image)

    plt.subplot(n_examples, 2, i * 2 + 2)
    mask = mi.imread(train_mask_files[idx])
    plt.imshow(mask)

plt.show()


def dice_coef(y_true, y_pred, smooth=0.9):
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = K.sum(y_true_f * y_pred_f)
    return (2.0 * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)


def dice_coef_loss(y_true, y_pred):
    return -dice_coef(y_true, y_pred)


def construct_model(img_rows, img_cols, img_channels):
    inputs = Input((img_rows, img_cols, img_channels))

    conv1 = Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(inputs)
    conv1 = Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv1)
    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)

    conv2 = Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool1)
    conv2 = Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)

    conv3 = Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool2)
    conv3 = Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv3)
    pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)

    conv4 = Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool3)
    conv4 = Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv4)
    drop4 = Dropout(0.5)(conv4)
    pool4 = MaxPooling2D(pool_size=(2, 2))(drop4)

    conv5 = Conv2D(1024, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool4)
    conv5 = Conv2D(1024, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv5)
    drop5 = Dropout(0.5)(conv5)

    up6 = Conv2D(512, 2, activation='relu', padding='same', kernel_initializer='he_normal')(
        UpSampling2D(size=(2, 2))(drop5))

    merge6 = concatenate([drop4, up6], axis=3)
    conv6 = Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge6)
    conv6 = Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv6)

    up7 = Conv2D(256, 2, activation='relu', padding='same', kernel_initializer='he_normal')(
        UpSampling2D(size=(2, 2))(conv6))
    merge7 = concatenate([conv3, up7], axis=3)
    conv7 = Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge7)
    conv7 = Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv7)

    up8 = Conv2D(128, 2, activation='relu', padding='same', kernel_initializer='he_normal')(
        UpSampling2D(size=(2, 2))(conv7))
    merge8 = concatenate([conv2, up8], axis=3)
    conv8 = Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge8)
    conv8 = Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv8)

    up9 = Conv2D(64, 2, activation='relu', padding='same', kernel_initializer='he_normal')(
        UpSampling2D(size=(2, 2))(conv8))
    merge9 = concatenate([conv1, up9], axis=3)
    conv9 = Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge9)
    conv9 = Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv9)
    conv9 = Conv2D(2, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv9)
    conv10 = Conv2D(1, 1, activation='sigmoid')(conv9)

    return Model(inputs=inputs, outputs=conv10)


# Generate Model
IMG_ROWS = 256
IMG_COLS = 256
IMG_CHANNELS = 3

model = construct_model(IMG_ROWS, IMG_COLS, IMG_CHANNELS)
opt = Adam()
model.compile(optimizer=opt, loss=dice_coef_loss, metrics=[dice_coef])
model.summary()

X = np.ndarray((len(train_image_files), IMG_ROWS, IMG_COLS, IMG_CHANNELS), dtype=np.uint8)
y = np.ndarray((len(train_mask_files), IMG_ROWS, IMG_COLS, 1), dtype=np.uint8)

train_file_size = len(train_image_files)

for i in range(train_file_size):
    img = cv2.imread(train_image_files[i])
    img = cv2.resize(img, (IMG_COLS, IMG_ROWS))
    label = mi.imread(train_mask_files[i])
    label = cv2.cvtColor(label, cv2.COLOR_BGRA2GRAY)
    label = cv2.resize(label, (IMG_COLS, IMG_ROWS))
    label = label[:, :].reshape((IMG_COLS, IMG_ROWS, 1))
    img = np.array([img / 255.])
    label = np.array([label])
    X[i] = img
    y[i] = label

batch_size = 4
n_epochs = 50

history = model.fit(X, y, batch_size=batch_size, epochs=n_epochs,
                    verbose=1, shuffle=True, validation_split=0.1)

MODELS_PATH = "E:/OneDrive/data_ai/model_unet/"
if not os.path.exists(MODELS_PATH):
    os.mkdir(MODELS_PATH)

ts = str(int(time.time()))
model_path = os.path.join(MODELS_PATH, f'model-{ts}.h5')
model.save(model_path)

history_path = os.path.join(MODELS_PATH, f'model-{ts}.history')
pickle.dump(history.history, open(history_path, "wb"))

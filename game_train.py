import game1 as g
from collections import deque
import os
import numpy as np

# INIT parameter
DATA_DIR = "./data"
NUM_ACTIONS = 3 # number of actions, (left, stay, right)
GAMMA = 0.99
INITIAL_EPSILON = 0.1 # starting value of epsilon
FINAL_EPSILON = 0.0001 # final value of epsilon
MEMORY_SIZE = 50000 # number of previous transitions to remember
NUM_EPOCHS_OBSERVE = 200
NUM_EPOCHS_TRAIN = 5000

BATCH_SIZE = 32
NUM_EPOCHS = NUM_EPOCHS_OBSERVE + NUM_EPOCHS_TRAIN

# Network
model = g.cnn_model()

game = g.WrappedGame1(no_screen=True)
experience = deque(maxlen=MEMORY_SIZE)
fout = open(os.path.join(DATA_DIR, "rl-network-result.csv"), "w")
num_games, num_wins = 0, 0
epsilon = INITIAL_EPSILON

for e in range(NUM_EPOCHS):
    loss = 0.0
    game.reset()

    # get the first state
    a_0 = 1 #(0: left, 1: stay, 2: right)
    x_t, r_0, game_over = game.step(a_0)
    s_t = g.preprocess_images(x_t)

    while not game_over:
        s_tml = s_t

        # next action
        if e <= NUM_EPOCHS_OBSERVE:
            a_t = np.random.randint(low=0, high=NUM_ACTIONS, size=1)[0]
        else:
            if np.random.rand() <= epsilon:
                a_t = np.random.randint(low=0, high=NUM_ACTIONS, size=1)[0]
            else:
                q = model.predict(s_t)[0]
                a_t = np.argmax(q)

        # apply action & get reward
        x_t, r_t, game_over = game.step(a_t)
        s_t = g.preprocess_images(x_t)
        if r_t == 1:
            num_wins += 1

        experience.append((s_tml, a_t, r_t, s_t, game_over))

        if e > NUM_EPOCHS_OBSERVE:
            X, Y = g.get_next_batch(experience, model, NUM_ACTIONS,
                                    GAMMA, BATCH_SIZE)
            loss_ = model.train_on_batch(X, Y)
            loss += loss_

    if epsilon > FINAL_EPSILON:
        epsilon -= (INITIAL_EPSILON - FINAL_EPSILON)/NUM_EPOCHS

    print(f"Epoch {e+1:04d}/{NUM_EPOCHS:d} | Loss {loss:.5f} | Win Count {num_wins:d}")
    fout.write(f"{e+1:04d}, {loss:.5f}, {num_wins:d}\n")

    if e > 1 and e % 200 == 0:
        model.save(os.path.join(DATA_DIR, f"rl-network-{e:d}.h5"), overwrite=True)

fout.close()
model.save(os.path.join(DATA_DIR, "rl-network-last.h5"), overwrite=True)



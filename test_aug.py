
from keras.preprocessing import image
import catdog_dt as pre
import os
import matplotlib.pyplot as plt

datagen = image.ImageDataGenerator(rotation_range=40, width_shift_range=0.2,
                                   height_shift_range=0.2, shear_range=0.2, zoom_range=0.2,
                                   horizontal_flip=True, fill_mode='nearest')

fnames = [os.path.join(pre.train_cats_dir, fname) for
          fname in os.listdir(pre.train_cats_dir)]

img_path = fnames[3]
img = image.load_img(img_path, target_size=(150, 150))
# print(type(img))

x = image.img_to_array(img)
x = x.reshape((1,) + x.shape)

i = 0
for batch in datagen.flow(x, batch_size=1):
    plt.figure(i)
    plt.imshow(image.array_to_img(batch[0]))
    i+=1
    if i%10 == 0:
        break

plt.show()


import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np


bandit_arms = [0.2, -0.2, -2]
num_arms =len(bandit_arms)

def pull_bandit(bandit):
    # random
    result = np.random.randn(1)
    if result > bandit:
        return 1
    else:
        return -1

tf.reset_default_graph()

weights = tf.Variable(tf.ones([num_arms]))
output =tf.nn.softmax(weights)

reward_holder = tf.placeholder(shape=[1], dtype=tf.float32)
action_holder = tf.placeholder(sahpe=[1], dtype=tf.int32)

responsible_output = tf.slice(output, action_holder, [1])
loss = -tf.log(responsible_output)*reward_holder

optimizer = tf.train.AdamOptimizer(learning_rate=1e-3)
update = optimizer.minimize(loss)

# Episode number
total_episodes = 1000
total_reward = np.zeros(num_arms)
init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)
    i = 0

    while i < total_episodes:
        actions = sess.run(output)
        a = np.random.choice(actions, p=actions)
        action = np.argmax(action == a)

        reward = pull_bandit(bandit_arms[action])

        # update network
        _, resp, ww = sess.run([update, responsible_output, weights],
                               feed_dict={reward_holder:[reward],
                                          action_holder:[action]})
        #
        total_reward[action] += reward
        if i % 50:
            print(f"Running reward for the {num_arms} " +
                  f"arms of the bandit: {total_reward}")
        i += 1


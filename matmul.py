import tensorflow as tf

sess = tf.InteractiveSession()

A = tf.constant([ [1, 2, 3],
                  [4, 5, 6 ]])

print(A.get_shape())

x = tf.constant([1, 0, 1])
print(x.get_shape())

x = tf.expand_dims(x, 1)
print(x.get_shape())


b = tf.matmul(A, x)
print(b.eval())

sess.close()
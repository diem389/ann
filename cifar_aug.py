
from keras.datasets import cifar10
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.optimizers import SGD, Adam, RMSprop
from keras.preprocessing.image import ImageDataGenerator


import matplotlib.pyplot as plt

import os
from keras.callbacks import ModelCheckpoint


def test_model(model):
    score = model.evaluate(X_test, Y_test, batch_size=BATCH_SIZE, verbose=VERBOSE)
    print("Test Score: ", score[0])
    print("Test Accuracy: ", score[1])

datagen = ImageDataGenerator(rotation_range=40,
                             width_shift_range=0.2, height_shift_range=0.2,
                             zoom_range=0.2, horizontal_flip=True, fill_mode='nearest')


IMG_CHANNELS = 3
IMG_ROWS = 32
IMG_COLS = 32

BATCH_SIZE = 128
NB_EOPCH = 20
NB_CLASSES = 10
VERBOSE = 1
MODEL_DIR = "/mnt/onetera/dev_ai/DeepLearningModels"
OPTIM = Adam()

checkpoint = ModelCheckpoint(filepath=os.path.join(MODEL_DIR, "cifar_model_best.h5"),
                             save_best_only=True)

# load data set
(X_train, y_train), (X_test, y_test) = cifar10.load_data()
Y_train = np_utils.to_categorical(y_train, NB_CLASSES)
Y_test = np_utils.to_categorical(y_test, NB_CLASSES)
X_train = X_train.astype('float32')/255
X_test = X_test.astype('float32')/255

print('X_train shape:', X_train.shape)
print(X_train.shape[0], 'train samples')
print(X_test.shape[0], 'test samples')

# Network

model = Sequential()

model.add(Conv2D(32, kernel_size=3, padding='same',
                 input_shape=(IMG_ROWS, IMG_COLS, IMG_CHANNELS)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Flatten())

model.add(Dense(512))
model.add(Activation('relu'))
model.add(Dropout(0.5))

model.add(Dense(NB_CLASSES))
model.add(Activation('softmax'))
model.summary()

# Train

model.compile(loss='categorical_crossentropy', optimizer=OPTIM, metrics=['accuracy'])
model.fit(X_train, Y_train, batch_size=BATCH_SIZE, epochs=NB_EOPCH,
          validation_split=0.2, verbose=VERBOSE, callbacks=[checkpoint])

# Save last model
model_json = model.to_json()
open('cifar10_archi.json', 'w').write(model_json)
model.save_weights('cifar10_weights.h5', overwrite=True)

#
print("Test on the last model...")
test_model(model)

print("Test on the best saved model...")
filepath = os.path.join(MODEL_DIR, "cifar_model_best.h5")
from keras.models import load_model
model2 = load_model(filepath)
test_model(model2)



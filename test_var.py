import tensorflow as tf

init_var = tf.random_normal((1,5), 0, 1)
var = tf.Variable(init_var, name='var')

print(f"pre run: \n{var}")

init = tf.global_variables_initializer()
with tf.Session() as sess:
    sess.run(init)
    post_var = sess.run(var)

print(f"\npost run: \n{post_var}")
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from keras.models import Sequential
from keras.layers import Dense, LSTM, Dropout
from sklearn.preprocessing import MinMaxScaler

# importing training set


def generate_xy(training_set):
    # Feature Scaling
    sc = MinMaxScaler(feature_range=(0, 1))
    training_set_scaled = sc.fit_transform(training_set)

    X_trains = []
    y_trains = []

    for i in range(60, 1250):
        X_trains.append(training_set_scaled[i-60:i, 0])
        y_trains.append(training_set_scaled[i, 0])

    X_trains, y_trains = np.array(X_trains), np.array(y_trains)
    X_trains = np.reshape(X_trains, (X_trains.shape[0:2] + (1,)))
    return X_trains, y_trains, sc


def create_regressor(input_shape_):
    model = Sequential()
    model.add(LSTM(units=50, return_sequences=True, input_shape=input_shape_))
    model.add(Dropout(0.2))

    model.add(LSTM(units=50, return_sequences=True))
    model.add(Dropout(0.2))

    model.add(LSTM(units=50, return_sequences=True))
    model.add(Dropout(0.2))

    model.add(LSTM(units=50))
    model.add(Dropout(0.2))

    model.add(Dense(1))
    model.compile(optimizer='adam', loss='mean_squared_error')
    return model


if __name__ == "__main__":

    dataset_train = pd.read_csv('data/Google_Stock_Price_Train.csv')
    training_set = dataset_train.iloc[:, 1:2].values
    X_trains, y_trains, sc = generate_xy(training_set)

    regressor = create_regressor((X_trains.shape[1], 1))
    regressor.fit(X_trains, y_trains, epochs=100, batch_size=32)
    regressor.save("result.h5")

    # Finished
    print("Train Finished")
    

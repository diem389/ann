import os
import numpy as np
from keras.preprocessing.image import ImageDataGenerator
from catdog_dt import train_dir, valid_dir, test_dir

from keras.layers import Input, Conv2D, \
    MaxPooling2D, Dense, Flatten, Dropout
from keras.models import Model
from keras import optimizers
from keras.applications import VGG16
import graph_util as gu

conv_base = VGG16(weights='imagenet', include_top=False,
                  input_shape=(150, 150, 3))
print("--- conv_base summary ---")
conv_base.summary()

datagen = ImageDataGenerator(rescale=1.0 / 255)
batch_size = 20


def extract_features(directory, sample_count):
    features = np.zeros(shape=(sample_count, 4, 4, 512))
    labels = np.zeros(shape=(sample_count,))
    generator = datagen.flow_from_directory(directory,
                                            target_size=(150, 150),
                                            batch_size=batch_size,
                                            class_mode='binary')

    i = 0
    for inputs_batch, labels_batch in generator:
        features_batch = conv_base.predict(inputs_batch)
        features[i * batch_size: (i + 1) * batch_size] = features_batch
        labels[i * batch_size: (i + 1) * batch_size] = labels_batch
        i += 1
        if i * batch_size >= sample_count:
            break

    return features, labels


train_features, train_labels = extract_features(train_dir, 2000)
validation_features, validation_labels = extract_features(valid_dir, 1000)
test_features, test_labels = extract_features(test_dir, 1000)

# Flattening
train_features = np.reshape(train_features, (2000, 4*4*512))
validation_features = np.reshape(validation_features, (1000, 4*4*512))
test_features = np.reshape(test_features, (1000, 4*4*512))

def cnn_model_for_vgg(input_dim=4*4*512):
    input = Input((input_dim,))
    x = Dense(256, activation='relu')(input)
    x = Dropout(0.5)(x)
    output = Dense(1, activation='sigmoid')(x)
    m = Model(inputs=input, outputs=output)
    m.compile(optimizer=optimizers.RMSprop(lr=2.0e-5),
              loss='binary_crossentropy', metrics=['acc'])
    return m

model = cnn_model_for_vgg()
history = model.fit(train_features, train_labels,
                    epochs=30, batch_size=20,
                    validation_data=(validation_features, validation_labels))

gu.draw_result(history.history)
# Module Objective: Predict next char from 10 previous character with RNN
# 1. Load Libs

from keras.layers import Dense, Activation
from keras.layers.recurrent import SimpleRNN
from keras.models import Sequential
from keras.utils.vis_utils import plot_model
import numpy as np

# 2. Load Data
fin = open("../data/alice.txt", 'r')
lines = []
for line in fin:
    line = line.strip().lower()
    # line = line.decode('ascii', 'ignore')
    if len(line) == 0:
        continue
    lines.append(line)
fin.close()
text = " ".join(lines)

# 3. Generate look up table
chars = set([c for c in text])
nb_chars = len(chars)
char2index = dict((c, i) for i, c in enumerate(chars))
index2char = dict((i, c) for i, c in enumerate(chars))

# 4. Create input & label texts
#
#   it turned into a pig would look like this:
#   it turned -> i
#   t turned i -> n
#   turned in -> t
#   turned int -> o
#   urned into ->
#   rned into -> a
#   ned into a ->
#   ed into a -> p
#   d into a p -> i
#   into a pi -> g

SEQLEN = 10
STEP = 1

input_chars = []
label_chars = []
for i in range(0, len(text) - SEQLEN, STEP):
    input_chars.append(text[i:i + SEQLEN])
    label_chars.append(text[i + SEQLEN])  # Target or Label

# 5. Prepare input data
# - One Hot Encoded
X = np.zeros((len(input_chars), SEQLEN, nb_chars), dtype=np.bool)
y = np.zeros((len(label_chars), nb_chars), dtype=np.bool)
for i, input_char in enumerate(input_chars):
    for j, ch in enumerate(input_char):
        X[i, j, char2index[ch]] = 1
    y[i, char2index[label_chars[i]]] = 1

# 6. Hyper parameter setup
HIDDEN_SIZE = 128
BATCH_SIZE = 128
NUM_ITERATION = 25
NUM_EPOCHS_PER_ITERATION = 1
NUM_PREDS_PER_EPOCH = 100

# 7. Make a model
model = Sequential()
model.add(SimpleRNN(HIDDEN_SIZE, return_sequences=False,
                    input_shape=(SEQLEN, nb_chars), unroll=True))
#  unroll=True - speed up the training
model.add(Dense(nb_chars))
model.add(Activation('softmax'))  # Categorical activation
model.compile(loss='categorical_crossentropy', optimizer='rmsprop')
model.summary()

# 8. Training & Verification
f_test = open("../data/alice_test_result.txt", 'w')


def print2(str='', end='\n', file=f_test):
    file.write(str)
    file.write(end)

for iteration in range(NUM_ITERATION):
    print2("=" * 50)
    print2("Iteration #: %d" % (iteration))
    model.fit(X, y, batch_size=BATCH_SIZE, epochs=NUM_EPOCHS_PER_ITERATION)
    # Test
    test_idx = np.random.randint(len(input_chars))
    test_chars = input_chars[test_idx]
    for i in range(NUM_PREDS_PER_EPOCH):
        Xtest = np.zeros((1, SEQLEN, nb_chars))
        for j, ch in enumerate(test_chars):
            Xtest[0, j, char2index[ch]] = 1
        pred = model.predict(Xtest, verbose=0)[0]  # 1 row matrix
        ypred = index2char[np.argmax(pred)]
        print2(ypred, end='')
        # Move forward
        test_chars = test_chars[1:] + ypred
    print2()
print()
f_test.close()

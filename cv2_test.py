import cv2
import matplotlib.image as mi

train_file1 = "e:\\OneDrive\\data_ai\\carvana_unet\\train\\"
mask_file1 = "e:\\OneDrive\\data_ai\\carvana_unet\\train_masks\\"

img1 = cv2.imread(train_file1 + "0cdf5b5d0ce1_01.jpg")
img2 = mi.imread(mask_file1 + "0cdf5b5d0ce1_01_mask.gif")

print("img1.shape: ", img1.shape)
print("img2.shape: ", img2.shape)

img2_gray = cv2.cvtColor(img2, cv2.COLOR_BGRA2GRAY)
print("img2_gray.shape: ", img2_gray.shape)

import numpy as np
import cv2
import matplotlib.pyplot as plt
import glob
import platform
import os

DATA_DIR = ""

if platform.platform().startswith("Linux"):
    DATA_DIR = os.path.expanduser("~/data/images/")
else:
    DATA_DIR = "E:/data/augmentation/"

images = glob.glob(DATA_DIR + '*')
print("Data Folder: ", DATA_DIR)
print("Loaded images: ", len(images))


def plot_images(image, function, *args):
    plt.figure(figsize=(10, 10))
    n_examples = 9
    for i in range(n_examples):
        img = cv2.imread(image)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img = function(img, *args)
        plt.subplot(3, 3, i + 1)
        plt.imshow(img)
    plt.show()


def rotate_image(image, rotate=20):
    width, height, _ = image.shape
    random_rotation = np.random.uniform(low=-rotate, high=rotate)
    M = cv2.getRotationMatrix2D((width / 2, height / 2), random_rotation, 1)
    return cv2.warpAffine(image, M, (height, width))


# Adjust color
def adjust_brightness(image, brightness=60):
    rand_brightness = np.random.uniform(low=-brightness, high=brightness)
    return cv2.add(image, rand_brightness)


def random_shifts(image, shift_max_x=100, shift_max_y=100):
    width, height, _ = image.shape
    shift_x = np.random.randint(low=-shift_max_x, high=shift_max_x)
    shift_y = np.random.randint(low=-shift_max_y, high=shift_max_y)
    M = np.float32([[1, 0, shift_x], [0, 1, shift_y]])
    return cv2.warpAffine(image, M, (height, width))


# plot_images(images[17], rotate_image, 40)
plot_images(images[4], random_shifts, 150, 30)

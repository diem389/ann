

# 1. Importing libs
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler

from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential
from keras.callbacks import TensorBoard

# 2. Load Data
data = pd.read_csv('../data/nasdaq_stock.csv')
data = data[['Open', 'High', 'Low', 'Volume', 'Close']]
data.head()

# 3. Hyper-parameter setup
sequence_length = 21
n_features = len(data.columns)
val_ratio = 0.1 # splitting ratio
n_epochs = 300
batch_size = 512

# 4. data --> Numpy array
data = data.values
data_processed = []
for index in range(len(data) - sequence_length):
    data_processed.append(data[index: index+sequence_length])
data_processed = np.array(data_processed)

# 5. Data Splitting
val_split = round((1-val_ratio)*data_processed.shape[0])
train = data_processed[:int(val_split), :]
val = data_processed[int(val_split):, :]

print("Training data: {}".format(train.shape))
print("Validation data: {}".format(val.shape))

# 6. Scaling data
train_samples, train_nx, train_ny = train.shape
val_samples, val_nx, val_ny = val.shape

train = train.reshape((train_samples, train_nx*train_ny))
val = val.reshape((val_samples, val_nx*val_ny))
preprocessor = MinMaxScaler().fit(train)
train = preprocessor.transform(train)
val = preprocessor.transform(val)

train = train.reshape((train_samples, train_nx, train_ny))
val = val.reshape((val_samples, val_nx, val_ny))

# 7. Extraction, Change 21 to 20
X_train = train[:, :-1] # Tip: we can omit remaining column.
y_train = train[:, -1][:,-1]
X_val = val[:, :-1] # Tip: we can omit remaining column.
y_val = val[:, -1][:,-1]

X_train = np.reshape(X_train, (X_train.shape[0],
                               X_train.shape[1], n_features))
X_val = np.reshape(X_val, (X_val.shape[0], X_val.shape[1], n_features))

# 8. Define Model
model = Sequential()
model.add(LSTM(input_shape=(X_train.shape[1:]), units=128,
               return_sequences=True))
model.add(Dropout(0.5))
model.add(LSTM(units=128, return_sequences=False))
model.add(Dropout(0.25))
model.add(Dense(1))
model.add(Activation("linear"))

model.compile(loss="mse", optimizer="adam")
model.summary()

# 8.1 Callbacks setup
# callbacks = [TensorBoard(log_dir='logdir',
#                          histogram_freq=1, embeddings_data=1),]

# 9. Training
history = model.fit(X_train, y_train, batch_size=batch_size,
                    epochs=n_epochs, verbose=2)


# 10. Validation with predict
preds_val = model.predict(X_val)
diff = []
for i in range(len(y_val)):
    pred = preds_val[i][0]
    diff.append(y_val[i] - pred)

# 11. De-normalization of predicted values
real_min = preprocessor.data_min_[-1]
real_max = preprocessor.data_max_[-1]
preds_real = preds_val*(real_max-real_min) + real_min
y_val_real = y_val*(real_max-real_min) + real_min

# 12. Plot draw
plt.plot(preds_real, label='Predictions')
plt.plot(y_val_real, label='Actual values')
plt.xlabel('test')
plt.legend(loc=0)
plt.show()
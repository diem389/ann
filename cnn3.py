import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

from tensorflow.examples.tutorials.mnist import input_data

DATA_DIR = 'e:\\data\\mnist'

mnist = input_data.read_data_sets(DATA_DIR, one_hot=True)

train_x = []
for img in mnist.train.images:
    train_x.append(np.reshape(img, (28, 28)))
train_x = np.array(train_x, dtype=np.float32)

test_x = []
for img in mnist.train.images:
    test_x.append(np.reshape(img, (28, 28)))
test_x = np.array(test_x, dtype=np.float32)

train_labels = mnist.train.labels
test_labels = mnist.test.labels

# Set hyper params
learn_rate = 0.0005
evaluation_size = 500
batch_size = 50
image_width = train_x[0].shape[0]
image_height = train_x[0].shape[1]
target_size = max(train_labels) + 1
num_channels = 1
generations = 500
eval_every = 5
conv1_features = 25
conv2_features = 50
max_pool_size1 = 2
max_pool_size2 = 2

x_input_shape = (batch_size, image_width, image_height, num_channels) #???
x_input = tf.placeholder(tf.float32, shape=x_input_shape)
y_target = tf.placeholder(tf.float32, shape=(batch_size,))

eval_input_shape = (evaluation_size, image_width, image_height)
eval_input = tf.placeholder(tf.float32, shape=eval_input_shape)

with tf.Session() as sess:
    pass



import glob
import os
import numpy as np

from keras.models import Model, load_model
import cv2
import matplotlib.pyplot as plt

from keras import backend as K

IMG_ROWS = 256
IMG_COLS = 256
IMG_CHANNELS = 3


def dice_coef(y_true, y_pred, smooth=0.9):
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = K.sum(y_true_f * y_pred_f)
    return (2.0 * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)


def dice_coef_loss(y_true, y_pred):
    return -dice_coef(y_true, y_pred)


MODELS_PATH = "E:\\OneDrive\\data_ai\\model_unet\\"
lof = glob.glob(os.path.join(MODELS_PATH, "*.h5"))
print(lof)

lf = max(lof, key=os.path.getctime)
print("latest: \n", lf)

model = load_model(lf, custom_objects={'dice_coef_loss': dice_coef_loss, 'dice_coef': dice_coef})
x1 = cv2.imread("E:\\OneDrive\\data_ai\\car1.jpg")
plt.imshow(x1)
plt.show()

x1 = cv2.resize(x1, (IMG_COLS, IMG_ROWS))
x1 = np.array([x1/255.])
y1 = model.predict([x1])

print("y1 shape: ", y1.shape)
cv2.imwrite("E:\\OneDrive\\data_ai\\car1_mask.png", y1[0])
plt.imshow(y1[0])

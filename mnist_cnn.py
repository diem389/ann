from keras.datasets import mnist
from keras.models import Model
from keras import layers
from keras.utils import to_categorical
import matplotlib.pyplot as plt

(train_images, train_labels), (test_images, test_labels) = mnist.load_data()


# Sequential
# network = models.Sequential()
# network.add(layers.Dense(512, activation='relu', input_shape=(28*28,)))
# network.add(layers.Dense(10, activation='softmax'))
#

# Functional style

def cnn_model(img_row=28, img_col=28, img_chnnel=1):
    input = layers.Input((img_row, img_col, img_chnnel))

    conv1 = layers.Conv2D(32, (3, 3), activation='relu')(input)
    conv1 = layers.MaxPooling2D((2, 2))(conv1)

    conv2 = layers.Conv2D(64, (3, 3), activation='relu')(conv1)
    conv2 = layers.MaxPooling2D((2, 2))(conv2)

    conv3 = layers.Conv2D(64, (3, 3))(conv2)

    output = layers.Flatten()(conv3)
    output = layers.Dense(64, activation='relu')(output)
    output = layers.Dense(10, activation='softmax')(output)
    model = Model(inputs=input, outputs=output)
    model.compile(optimizer='rmsprop',
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    return model


# Data preprocessing
train_images = train_images.reshape((60000, 28, 28, 1))
train_images = train_images.astype('float32') / 255.0

test_images = test_images.reshape((10000, 28, 28, 1))
test_images = test_images.astype('float32') / 255.0


train_labels = to_categorical(train_labels)
test_labels = to_categorical(test_labels)

cnn = cnn_model()
cnn.fit(train_images, train_labels, epochs=5, batch_size=128)
# Test & Verification
test_loss, test_acc = cnn.evaluate(test_images, test_labels)
cnn.summary()
print('Test acc:', test_acc)
print('Loss: ', test_loss)

# try draw

digit = train_images[4]
digit = digit.reshape((28, 28))
plt.imshow(digit, cmap=plt.cm.binary)
plt.show()

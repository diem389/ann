from keras.layers import Input, Lambda, Dense, Flatten
from keras.models import Model
from keras.applications.vgg16 import VGG16, preprocess_input
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator

from sklearn.metrics import confusion_matrix
import numpy as np
import matplotlib.pyplot as plt

from glob import glob

IMAGE_SIZE = [100, 100]
epochs = 5
batch_size = 128

train_path = "../Data/fruits-360-small/Training"
valid_path = "../Data/fruits-360-small/Test"

train_image_files = glob(train_path + "/*/*.jp*g")
valid_image_files = glob(valid_path + "/*/*.jp*g")
folders = glob(train_path + "/*")

plt.imshow(image.load_img(np.random.choice(train_image_files)))
plt.show()

vgg = VGG16(input_shape=IMAGE_SIZE + [3],
            weights='imagenet', include_top=False)

for layer in vgg.layers:
    layer.trainable = False


x = Flatten()(vgg.output)
prediction = Dense(len(folders), activation='softmax')(x)

# Create model
model = Model(inputs=vgg.input, outputs=prediction)
model.summary()

model.compile(loss='categorical_crossentropy',
              optimizer='rmsprop', metrics=['accuracy'])

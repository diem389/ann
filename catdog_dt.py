import os, shutil

origin_path_root_win = 'e:/data/'
origin_path_root_linux = '/mnt/sda1/data/'

original_dataset_dir = origin_path_root_win + 'dogs-vs-cats/train'
base_dir = origin_path_root_win + 'dogs-vs-cats/small'

def make_dir(base, path):
    path_full = os.path.join(base, path)
    if not os.path.exists(path_full):
        os.mkdir(path_full)
    else:
        pass
    return path_full

# Make sub folders
train_dir = make_dir(base_dir, 'train')
test_dir = make_dir(base_dir, 'test')
valid_dir = make_dir(base_dir, 'validation')

train_cats_dir = make_dir(train_dir, 'cats')
train_dogs_dir = make_dir(train_dir, 'dogs')

test_cats_dir = make_dir(test_dir, 'cats')
test_dogs_dir = make_dir(test_dir, 'dogs')

valid_cats_dir = make_dir(valid_dir, 'cats')
valid_dogs_dir = make_dir(valid_dir, 'dogs')

# Cats 
fnames = [f'cat.{i}.jpg' for i in range(1000)]
for fname in fnames:
    src = os.path.join(original_dataset_dir, fname)
    dst = os.path.join(train_cats_dir, fname)
    shutil.copyfile(src, dst)

fnames = [f'cat.{i}.jpg' for i in range(1000, 1500)]
for fname in fnames:
    src = os.path.join(original_dataset_dir, fname)
    dst = os.path.join(valid_cats_dir, fname)
    shutil.copyfile(src, dst)


fnames = [f'cat.{i}.jpg' for i in range(1500, 2000)]
for fname in fnames:
    src = os.path.join(original_dataset_dir, fname)
    dst = os.path.join(test_cats_dir, fname)
    shutil.copyfile(src, dst)

# dogs 
fnames = [f'dog.{i}.jpg' for i in range(1000)]
for fname in fnames:
    src = os.path.join(original_dataset_dir, fname)
    dst = os.path.join(train_dogs_dir, fname)
    shutil.copyfile(src, dst)

fnames = [f'dog.{i}.jpg' for i in range(1000, 1500)]
for fname in fnames:
    src = os.path.join(original_dataset_dir, fname)
    dst = os.path.join(valid_dogs_dir, fname)
    shutil.copyfile(src, dst)


fnames = [f'dog.{i}.jpg' for i in range(1500, 2000)]
for fname in fnames:
    src = os.path.join(original_dataset_dir, fname)
    dst = os.path.join(test_dogs_dir, fname)
    shutil.copyfile(src, dst)



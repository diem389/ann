# ANN

import keras

import numpy
import matplotlib.pyplot as plt
import pandas as pd

# imports
data_path = "../resource_ann/Churn_Modelling.csv"
dataset = pd.read_csv(data_path)

X = dataset.iloc[:,[2, 3]].values
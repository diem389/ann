import tensorflow as tf
import numpy as np
import time
import os
import matplotlib.pyplot as plt
import io

sess = tf.Session()

# Some parameters
batch_size = 50
generations = 100


def test1():
    identity_matrix = tf.diag([1.0, 1.0, 1.0])
    A = tf.truncated_normal([2, 3])
    B = tf.fill([2, 3], 5.0)
    C = tf.random_uniform([3, 2])
    D = tf.convert_to_tensor(np.array([[1.0, 2.0, 3.0], [-3.0, -7.0, -1.0], [0.0, 5.0, -2.0]]))

    print(sess.run(identity_matrix))
    print(sess.run(A))
    print(sess.run(B))
    print(sess.run(C))
    print(sess.run(D))


def test2():
    print(sess.run(tf.nn.relu([-3., 3., 10.])))
    # ReLu6 Test
    print(sess.run(tf.nn.relu6([-3.0, 3.0, 10.])))


def test3_loadiris():
    from sklearn import datasets
    iris = datasets.load_iris()
    print(len(iris.data))
    print(len(iris.target))


def test4():
    x_vals = np.array([1., 3.0, 5.0, 7.0, 9.])
    x_data = tf.placeholder(tf.float32)
    m_const = tf.constant(3.)
    my_product = tf.multiply(x_data, m_const)

    for x_val in x_vals:
        print(sess.run(my_product, feed_dict={x_data: x_val}))


def test5():
    my_arr = np.array([[1.0, 3., 5., 7., 9],
                       [-2., 0., 2., 4., 6.],
                       [-6, -3., 0., 3., 6.]], dtype=np.float32)
    x_vals = np.array([my_arr, my_arr + 1])
    x_data = tf.placeholder(tf.float32, shape=(3, 5))
    m1 = tf.constant([[1.], [0.], [-1.], [2.], [4.]])
    m2 = tf.constant([[2.]])
    a1 = tf.constant([[10.]])

    # Declare operations
    prod1 = tf.matmul(x_data, m1)
    prod2 = tf.matmul(prod1, m2)
    add1 = tf.add(prod2, a1)

    for x_val in x_vals:
        print("x_val = ...")
        print(x_val)
        print(sess.run(add1, feed_dict={x_data: x_val}))


def test11():
    if not os.path.exists('tensorboard'):
        os.makedirs('tensorboard')

    summary_writer = tf.summary.FileWriter('tensorboard', sess.graph)
    time.sleep(3)
    batch_size = 50
    generations = 100

    # Create sample input data
    x_data = np.arange(1000)/10.
    true_slope = 2.0
    y_data = x_data + true_slope + np.random.normal(loc=0.0, scale=25, size=1000)

    # Split the dataset into a train & test set
    train_ix = np.random.choice(len(x_data), size=int(len(x_data)*0.9), replace=False)
    x_data_train, y_data_train = x_data[train_ix], y_data[train_ix]

    test_ix = np.setdiff1d(np.arange(1000), train_ix)
    x_data_test, y_data_test = x_data[train_ix], y_data[train_ix]

    # Placeholders, variables, model, loss, optimization
    x_graph_input = tf.placeholder(tf.float32, [None])
    y_graph_input = tf.placeholder(tf.float32, [None])

    # Declare model
    m = tf.Variable(tf.random_normal([1], dtype=tf.float32), name='Slope')
    output = tf.multiply(m, x_graph_input, name='Batch_Multiplication')

    # Declare loss
    residuals = output - y_graph_input
    l2_loss = tf.reduce_mean(tf.abs(residuals), name='L2_Loss')

    # Optimizer
    my_optim = tf.train.GradientDescentOptimizer(0.01)
    train_step = my_optim.minimize(l2_loss)

    with tf.name_scope('Slope_Estimation'):
        tf.summary.scalar('Slope_Estimation', tf.squeeze(m))

    with tf.name_scope('Loss_and_Residuals'):
        tf.summary.histogram('Histogram_Errors', l2_loss)
        tf.summary.histogram('Histogram_Residuals', residuals)

    # Merging summary
    summary_op = tf.summary.merge_all()

    # Init. Variables
    init = tf.initialize_all_variables()
    sess.run(init)

    for i in range(generations):
        batch_indices = np.random.choice(len(x_data_train), size=batch_size)
        x_batch = x_data_train[batch_indices]
        y_batch = y_data_train[batch_indices]
        _, train_loss, summary = sess.run([train_step, l2_loss, summary_op],
                                          feed_dict={x_graph_input: x_batch,
                                                     y_graph_input: y_batch})

        test_loss, test_resids = sess.run([l2_loss, residuals],
                                          feed_dict={x_graph_input: x_data_test,
                                                     y_graph_input: y_data_test})
        if (i+1)%10 == 0:
            print(f"Generation {i+1} of {generations}. "+
                  f"Train Loss: {train_loss:.3}, "+
                  f"Test Loss: {test_loss:.3}")

        log_writer = tf.summary.FileWriter('tensorboard')
        log_writer.add_summary(summary, i)
        time.sleep(0.5)

    # Add Image to TensorBoard (plot the linear fit!)
    slope = sess.run(m)
    plot_buf = gen_linear_plot(slope[0], x_data, y_data)
    image = tf.image.decode_png(plot_buf.getvalue(), channels=4)
    image = tf.expand_dims(image, 0)

    # Add image summary
    image_summary_op = tf.summary.image("Linear_Plot", image)
    image_summary = sess.run(image_summary_op)
    log_writer.add_summary(image_summary, i)
    log_writer.close()

# Protobuf
def gen_linear_plot(slope, x_data, y_data):
    linear_prediction = x_data*slope
    plt.plot(x_data, y_data, 'b.', label='data')
    plt.plot(x_data, linear_prediction, 'r-', linewidth=3, label="predicted line")
    plt.legend(loc='upper left')
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    # init address of start
    buf.seek(0)
    return (buf)


def test_12_genetic():
    pop_size = 100
    features = 50
    selection = 0.2
    mutation = 1./features
    



if __name__ == "__main__":
    test11()

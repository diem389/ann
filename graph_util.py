import matplotlib.pyplot as plt

# Result check
def draw_result(history_dict):
    plt.clf()

    loss_values = history_dict['loss']
    val_loss_values = history_dict['val_loss']
    acc = history_dict['acc']
    val_acc = history_dict['val_acc']

    epochs = range(1, len(loss_values) + 1)

    plt.subplot(2, 1, 1)
    plt.plot(epochs, loss_values, 'bo', label='Training loss')
    plt.plot(epochs, val_loss_values, 'b', label='Validation loss')

    plt.title('Training Loss & Acc.')
    plt.ylabel('Loss')
    plt.legend()

    plt.subplot(2, 1, 2)

    plt.plot(epochs, acc, 'ro', label='Training accuracy')
    plt.plot(epochs, val_acc, 'r', label='Validation accuracy')

    # plt.title('Training validation accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.legend()

    plt.show()


# draw_result(history.history)
from __future__ import division, print_function

from keras.models import load_model
from keras.optimizers import Adam
# from scipy.misc import imresize
import numpy as np
import os
import game1 as g

DATA_DIR = "./data"
MODEL_FILE = "rl-network-last.h5"

NUM_EPOCHS = 10

model = load_model(os.path.join(DATA_DIR, MODEL_FILE))
model.compile(optimizer=Adam(lr=1e-6), loss="mse")

game = g.WrappedGame1()

num_games, num_wins = 0, 0

for e in range(NUM_EPOCHS):
    loss = 0.0
    game.reset()

    # Get its first state
    a_0 = 1
    x_t, r_0, game_over = game.step(a_0)
    s_t = g.preprocess_images(x_t)

    while not game_over:
        s_tm1 = s_t
        q = model.predict(s_t)[0]
        a_t = np.argmax(q)
        # apply action & get reward
        x_t, r_t, game_over = game.step(a_t)
        s_t = g.preprocess_images(x_t)

        if r_t == 1:
            num_wins += 1

    num_games += 1
    print(f"Game: {num_games}, Wins: {num_wins:03d}")

print("Play Ended...")
